const http = require('http');
const url = require('url');
const commandLineArgs = require('command-line-args');

const optionDefinitions = [
    { name: 'port', alias: 'p', type: Number, defaultValue: 3000}
];
const options = commandLineArgs(optionDefinitions);
const PORT = options.port;

let requestCount = 0;

const server = http.createServer((req, res) => {
    const parsedUrl = url.parse(req.url, true)

    if(parsedUrl.pathname === '/') {
        requestCount++
    }

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({message: "Request handled successfully", requestCount }))
})

server.listen(PORT, 'localhost', (error) => {
    error ? console.log(error) : console.log(`Server is running ${PORT}`);
})

process.on('exit', () => {
    requestCount = 0;
})
